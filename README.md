[![Build Status](https://github.com/Sciss/Wr_t_ng-M_ch_n_/workflows/Scala%20CI/badge.svg?branch=main)](https://github.com/Sciss/Wr_t_ng-M_ch_n_/actions?query=workflow%3A%22Scala+CI%22)

# Wr_t_ng-M_ch_n_

This repository contains code for an art project.

(C)opyright 2017–2022 by Hanns Holger Rutz. All rights reserved. This project is released under the
[GNU General Public License](https://github.com/Sciss/Wr_t_ng-M_ch_n_/blob/main/LICENSE) v3+ and
comes with absolutely no warranties.
To contact the author, send an e-mail to `contact at sciss.de`.

## building

Builds with sbt against Scala 2.12.
